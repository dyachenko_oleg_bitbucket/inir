<?php

/**
 * @return string
 */
function theme_kbe_main() {
  $object_types = kbe_object_types();

  $rows = array();
  foreach ($object_types as $code => $object_type) {
    $rows[] = array(
      $object_type['name_plural'],
      l('Go to list', KBE . '/' . $code),
    );
  }

  return
    theme('table', array(
      'header' => array(
        'Name',
        'Action'
      ),
      'rows'   => $rows,
    ));
}

/**
 * @param $variables
 * @return string
 */
function theme_kbe_list($variables) {
  $object_type = $variables['object_type'];

  $output = theme('table', array(
    'header' => array(
      l('Back to Home', KBE),
      l('New', KBE . '/' . $object_type . '/new'),
    ),
  ));

  // list
  $list = array();

  // get from DB
  if ($class = kbe_classes('default', $object_type)) {
    $list = Object::getList($class);
  }

  // theme list
  $rows = array();
  foreach ($list as $id => $label) {
    $rows[] = array(
      $label,
      l('view', KBE . '/' . $object_type . '/' . $id . '/view'),
      l('edit', KBE . '/' . $object_type . '/' . $id . '/edit'),
    );
  }
  $output .= theme('table', array(
    'header' => array(
      'Label',
      'View',
      'Edit'
    ),
    'rows'   => $rows,
  ));

  return $output;
}


