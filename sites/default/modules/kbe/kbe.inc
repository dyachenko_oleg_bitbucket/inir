<?php

/**
 * @return array
 */
function kbe_object_types() {
  return array(
    'domain'    => array(
      'name'        => 'Domain',
      'name_plural' => 'Domains',
    ),
    'attribute' => array(
      'name'        => 'Attribute',
      'name_plural' => 'Attributes',
    ),
    'entity'    => array(
      'name'        => 'Entity',
      'name_plural' => 'Entities',
    ),
    'relation'  => array(
      'name'        => 'Relation',
      'name_plural' => 'Relations',
    ),
    'instance'  => array(
      'name'        => 'Instance',
      'name_plural' => 'Instances',
    ),

  );
}

/**
 * @return bool
 */
function kbe_mode_is_sync() {
  return TRUE; // todo
}

/**
 * @return array
 */
function kbe_drivers() {
  return array('mysql');
}

/**
 * @return null
 */
function kbe_driver() {
  return variable_get('kbe_driver', 'mysql');
}

/**
 *
 */
function kbe_initialize_objects() {
  // load default classes
  $default = kbe_classes('default');
  foreach ($default as $filename) {
    module_load_include('php', 'kbe', 'classes/default/' . $filename);
  }

  // load current driver's classes
  $driver = kbe_driver();
  foreach (kbe_classes($driver) as $filename) {
    module_load_include('php', 'kbe', 'classes/' . $driver . '/' . $filename);
  }
}

/**
 * @param null $group
 * @param null $code
 * @return array|bool
 */
function kbe_classes($group = NULL, $code = NULL) {
  $classes = array(
    'default' => array(
      'object'                      => 'Object',
      'db_driver_interface'         => 'DBDriverInterface',
      'db_driver'                   => 'DBDriver',
      // domain
      'domain_abstract'             => 'AbstractDomain',
      'domain'                      => 'Domain',
      // attributes
      'attribute_abstract'          => 'AbstractAttribute',
      'attribute'                   => 'Attribute',
      'attribute_attached'          => 'AbstractAttachedAttribute',
      // entities
      'entity_attribute'            => 'EntityAttribute',
      'entity_abstract'             => 'AbstractEntity',
      'entity'                      => 'Entity',
      // relations
      'relation_attribute'          => 'RelationAttribute',
      'relation_abstract'           => 'AbstractRelation',
      'relation'                    => 'Relation',
      // instance
      'instance_attribute_abstract' => 'AbstractInstanceAttribute',
      'instance_abstract'           => 'AbstractInstance',
      'instance_relation_abstract'  => 'AbstractInstanceRelation',
      'instance_attribute'          => 'InstanceAttribute',
      'instance'                    => 'Instance',
      'instance_relation'           => 'InstanceRelation',
      'instance_relation_attribute' => 'InstanceRelationAttribute',
    ),
    'mysql'   => array(
      'db_driver' => 'MysqlDriver',
    ),
  );

  // specific group
  if (!is_null($group)) {
    // specific class
    if (!is_null($code)) {
      return isset($classes[$group][$code]) ? $classes[$group][$code] : FALSE;
    }
    return isset($classes[$group]) ? $classes[$group] : array();
  }

  return $classes;
}

/**
 *
 */
function kbe_initialize_forms() {
  foreach (kbe_object_types() as $code => $object_type) {
    module_load_include('inc', 'kbe', 'includes/forms/form.' . $code);
  }
}