<?php

/**
 * Class Domain
 */
class Domain extends AbstractDomain {

  /**
   *
   */
  public function toString() {
    return $this->getName();
  }

  /**
   *
   */
  public function applyChanges() {
    $changes = $this->getChanges();

    foreach ($changes as $change) {
      switch ($change['field']) {
        case 'settings' :
          switch ($change['action']) {
            case 'delete' :
              unset($this->settings[$change['delta']]);
              break;
            case 'update' :
              $this->settings[$change['delta']] = $change['value'];
              break;
            case 'insert' :
              $this->settings[$change['delta']] = $change['value'];
              break;
          }
          break;
      }
    }
  }
}
