<?php


abstract class AbstractInstanceAttribute extends Object {

  /**
   * @var int
   */
  protected $oid = 0;

  /**
   * @var int
   */
  protected $oaid = 0;

  /**
   * @var int
   */
  protected $weight = 0;

  /**
   * @param int $oaid
   */
  public function setOaid($oaid) {
    $this->oaid = $oaid;
  }

  /**
   * @return int
   */
  public function getOaid() {
    return $this->oaid;
  }

  /**
   * @param int $oid
   */
  public function setOid($oid) {
    $this->oid = $oid;
  }

  /**
   * @return int
   */
  public function getOid() {
    return $this->oid;
  }

  /**
   * @param string $value
   */
  public function setValue($value) {
    $this->value = $value;
  }

  /**
   * @return string
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * @param int $weight
   */
  public function setWeight($weight) {
    $this->weight = $weight;
  }

  /**
   * @return int
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * @var string
   */
  protected $value = '';


}