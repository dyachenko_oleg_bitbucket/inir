<?php

/**
 *
 */
define('REVISION_STATUS_TEMP', 0);
define('REVISION_STATUS_LIVE', 1);
define('REVISION_STATUS_BLOCK', 2);

/**
 * Class Revision
 */
class Revision extends AbstractRevision {
  /**
   * @param $id
   */
  public function __construct($id = FALSE) {
    if (FALSE !== $id) {
      $this->load($id);
    }
  }

  /**
   * Load the class object
   *
   * @param $id
   */
  public function load($id) {
    $driver = self::$driver;
    $driver->load($this, $id);
  }

  /**
   * Saves the class object
   */
  public function save($edit = array()) {
    $driver = self::$driver;
    $driver->save($this, $edit);
  }

  /**
   *
   */
  protected function _save() {
    db_set_active('inir');

    db_query("REPLACE INTO {revisions} SET revision = :revision, uid = :uid, status = :status, timestamp = :timestamp, meta = :meta", array(
      ':revision'  => $this->getRevision(),
      ':uid'       => $this->getUid(),
      ':status'    => $this->getStatus(),
      ':timestamp' => $this->getDate(),
      ':meta'      => serialize($this->getMeta()),
    ));

    db_set_active();
  }
} 
