<?php


abstract class AbstractInstanceRelation extends Object {

  /**
   * @var int
   */
  protected $rid = 0;

  /**
   * @var int
   */
  protected $iid1 = 0;

  /**
   * @param int $iid1
   */
  public function setIid1($iid1) {
    $this->iid1 = $iid1;
  }

  /**
   * @return int
   */
  public function getIid1() {
    return $this->iid1;
  }

  /**
   * @param int $iid2
   */
  public function setIid2($iid2) {
    $this->iid2 = $iid2;
  }

  /**
   * @return int
   */
  public function getIid2() {
    return $this->iid2;
  }

  /**
   * @param int $rid
   */
  public function setRid($rid) {
    $this->rid = $rid;
  }

  /**
   * @return int
   */
  public function getRid() {
    return $this->rid;
  }

  /**
   * @var int
   */
  protected $iid2 = 0;

}