<?php

class RevisionChanges {

  /**
   * @var array
   */
  private $changes = array();

  /**
   * @param array $changes
   */
  public function setChanges($changes) {
    $this->changes = $changes;
  }

  /**
   * @return array
   */
  public function getChanges() {
    return $this->changes;
  }

  /**
   * @param $change
   */
  public function addChanges($change) {
    $this->changes[] = $change;
  }

  /**
   * @var null
   */
  private $revision = NULL;

  /**
   *
   */
  public function __construct (Revision $revision) {
    $this->revision = $revision;
  }

  /**
   *
   */
  public function save() {
    db_set_active('inir');

    foreach ($this->getChanges() as $change) {
      db_insert('domains')->fields($change + array(
          'id' => NULL,
          'revision' => $this->revision->getRevision(),
          'uid' => $this->revision->getUid(),
        ))->execute();
    }

    db_set_active();
  }

} 
