<?php

/**
 * Class Revision
 */
abstract class Object {

  /**
   * @var DBDriverDriver
   */
  public static $driver = NULL;

  /**
   * ID
   *
   * @var string
   */
  protected $id = NULL;

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Revision number
   *
   * @var string
   */
  protected $revision = NULL;

  /**
   * @param string $revision
   */
  public function setRevision($revision) {
    $this->revision = $revision;
  }

  /**
   * @return string
   */
  public function getRevision() {
    return $this->revision;
  }

  protected $changes = NULL;

  /**
   * @param array $changes
   */
  public function setChanges($changes) {
    $this->changes = $changes;
  }

  /**
   * @return array
   */
  public function getChanges() {
    if (is_null($this->changes)) {
      $this->changes = array();

      $driver = self::$driver;
      $this->changes = $driver->changes($this);

      $this->changes = array_reverse($this->changes);
    }
    return $this->changes;
  }

  /**
   *
   */
  public function applyChanges() {
    //
  }

  /**
   * @param $message
   */
  protected static function error($message) {
    die($message);
  }

  /**
   * Load the class object
   *
   * @param $id
   */
  public function load($id) {
    $driver = self::$driver;
    $driver->load($this, $id);
    // $this->error(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden.');
  }

  /**
   * Saves the class object
   */
  public function save($edit = array()) {
    self::$driver->save($this, $edit);
    // $this->error(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden.');
  }

  /**
   * @param $class
   * @return array
   */
  public static function getList($class) {
    return self::$driver->getList($class);
  }

  /**
   * @param string $language
   * @return mixed
   */
  public function getLabel($language = 'ru') {
    return self::$driver->getLabel($this, $language);
  }

  /**
   * @param $id
   */
  public function __construct($id = FALSE) {
    if (FALSE !== $id) {
      $this->load($id);
    }
  }

}
