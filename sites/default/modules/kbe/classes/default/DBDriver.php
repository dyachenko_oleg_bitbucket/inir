<?php

abstract class DBDriver implements DBDriverInterface {

  /**
   * @param Object $object
   * @param bool $object_id
   */
  public function load(Object $object, $object_id = FALSE) {
    die(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden');
  }

  /**
   * @param Object $object
   * @param array $edit
   */
  public function save(Object $object, $edit = array()) {
    die(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden');
  }

  /**
   * @param Object $object
   * @param bool $object_id
   */
  public function changes(Object $object) {
    die(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden');
  }
}