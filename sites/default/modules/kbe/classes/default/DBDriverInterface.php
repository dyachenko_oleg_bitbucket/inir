<?php

/**
 * Interface DBInterface
 */
interface DBDriverInterface {
  public function load(Object $object, $object_id = FALSE);
  public function save(Object $object, $edit = array());
  public function getList($class);
  public function getLabel(Object $object, $language = 'ru');
}