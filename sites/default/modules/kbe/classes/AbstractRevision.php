<?php

/**
 * Created by PhpStorm.
 * User: skullhole
 * Date: 16/04/14
 * Time: 11:20
 */
class AbstractRevision {

  /**
   * @var null
   */
  private $revision = NULL;

  /**
   * @var null
   */
  private $date = NULL;

  /**
   * @param null $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  /**
   * @return null
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @param null $revision
   */
  public function setRevision($revision) {
    $this->revision = $revision;
  }

  /**
   * @return null
   */
  public function getRevision() {
    return $this->revision;
  }

  /**
   * @param null $uid
   */
  public function setUid($uid) {
    $this->uid = $uid;
  }

  /**
   * @return null
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * @var null
   */
  private $uid = NULL;


  /**
   * @var int
   */
  private $status = NULL;

  /**
   * @param int $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return int
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param array $meta
   */
  public function setMeta($meta) {
    $this->meta = $meta;
  }

  /**
   * @return array
   */
  public function getMeta() {
    return $this->meta;
  }

  /**
   * @var array
   */
  private $meta = array();

  /**
   * @param null $revision
   */
  public function __construct($revision = NULL) {
    if (is_null($revision)) {
      $revision = $this->generateRevision();
    }
    $this->setRevision($revision);

    $this->load();
  }

  /**
   * @return string
   */
  private function generateRevision() {
    return md5(uniqid('revision_'));
  }

  /**
   *
   */
  protected function load() {
    die(__CLASS__ . '::' . __FUNCTION__ . ' must be overridden');
  }

  /**
   *
   */
  protected function save() {
    die(__CLASS__ . '::' . __FUNCTION__ . ' must be overridden');
  }
} 
