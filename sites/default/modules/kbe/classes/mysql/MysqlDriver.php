<?php

/**
 * Class MysqlDriver
 */
class MysqlDriver extends DBDriver {
  /**
   * @param $object
   * @param bool $id
   */
  public function load(Object $object, $id = FALSE) {
    db_set_active('inir');

    $class = get_class($object);
    switch ($class) {
      case 'Domain' :
        self::load_domain($object, $id);
        break;
      case 'Attribute' :
        self::load_attribute($object, $id);
        break;
      case 'EntityAttribute' :
        self::load_entity_attribute($object, $id);
        break;
      case 'Entity' :
        self::load_entity($object, $id);
        break;
      case 'RelationAttribute' :
        self::load_relation_attribute($object, $id);
        break;
      case 'Relation' :
        self::load_relation($object, $id);
        break;
      case 'InstanceAttribute' :
        self::load_instance_attribute($object, $id);
        break;
      case 'Instance' :
        self::load_instance($object, $id);
        break;
      case 'InstanceRelationAttribute' :
        self::load_instance_relation_attribute($object, $id);
        break;
      case 'InstanceRelation' :
        self::load_instance_relation($object, $id);
        break;

      case 'Revision' :
        self::load_revision($object, $id);
        break;

      default :
        d($class);
        break;
    }

    db_set_active();
  }

  /**
   * @param $object
   * @param array $edit
   */
  public function save(Object $object, $edit = array()) {
    d($object);
    dd($edit);
  }

  /**
   * @param Object $object
   *
   *
   *
   * CREATE TABLE revisions (
   * id INT NOT NULL AUTO_INCREMENT,
   * revision varchar(64) NOT NULL,
   * status int(11) NOT NULL DEFAULT '0',
   * uid int(11) NOT NULL,
   * timestamp int(11) NOT NULL DEFAULT '0',
   * meta text NOT NULL,
   * PRIMARY KEY (id)
   * ) ENGINE=InnoDB DEFAULT CHARSET=latin1

   */
  public function changes(Object $object) {
    db_set_active('inir');

    //
    $changes = array();
    $otype   = '';

    $class = get_class($object);
    switch ($class) {
      case 'Domain' :
        $otype = 'domain';
        break;
      default :
        d($class);
        break;
    }

    global $user;
    $results = db_query("
      SELECT rc.*
      FROM {revisions} r, {revisions} r2, {revisions_changes} rc
      WHERE
        r.revision = :revision AND
        r.uid = r2.uid AND
        r.uid = :uid AND
        r.timestamp >= r2.timestamp AND
        rc.revision = r2.revision AND
        rc.otype = :otype AND
        rc.oid = :oid
        ", array(
      ':revision' => $object->getRevision(),
      ':uid'      => $user->uid,
      ':otype'    => $otype,
      ':oid'      => $object->getId(),
    ));
    foreach ($results as $result) {
      $changes[] = (array)$result;
    }


    db_set_active();

    //
    return $changes;
  }


  /**
   * @param $object
   */
  public function getList($class) {
    db_set_active('inir');

    $list = array();

    switch ($class) {
      case 'Domain' :
        $list = self::list_domain();
        break;
      case 'Attribute' :
        $list = self::list_attribute();
        break;
      case 'Entity' :
        $list = self::list_entity();
        break;
      case 'Relation' :
        $list = self::list_relation();
        break;
      case 'EntityAttribute' :
        $list = self::list_entity_attribute();
        break;
      case 'RelationAttribute' :
        $list = self::list_relation_attribute();
        break;
      case 'Instance' :
        $list = self::list_instance();
        break;
      default :
//        Kint::trace();
        d($class);
        break;
    }

    db_set_active();

    return $list;
  }

  /**
   * Domain
   */

  /*

  DROP TABLE IF EXISTS domains;
  CREATE TABLE IF NOT EXISTS domains (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    name varchar(255) NOT NULL DEFAULT '' COMMENT 'Name.',
    type varchar(255) NOT NULL DEFAULT '' COMMENT 'Must be one of the following: bool, int, float, string, enum.',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY type (type),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for domains' ;

  DROP TABLE IF EXISTS domains_settings;
  CREATE TABLE IF NOT EXISTS domains_settings (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    did int(10) unsigned COMMENT 'Reference to domains.id',
    weight int(10) unsigned COMMENT 'weight value',
    value TEXT COMMENT 'Value',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY did (did),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for domain settings' ;

 */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_domain(Domain $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('domains', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: domain #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setType($result->type);
      $object->setName($result->name);
      $object->setRevision($result->revision);

      # settings
      $values = array();
      $list   = self::tablerows('domains_settings', array('did' => $object->getId()));
      foreach ($list as $item) {
        $values[$item->id] = $item->value;
      }
      $object->setSettings($values);
    }
  }

  /**
   * @return array
   */
  public static function list_domain() {
    $options = array();

    $results = db_query("SELECT * FROM domains");
    foreach ($results as $result) {
      $options[$result->id] = $result->name;
    }

    return $options;
  }

  /*

  DROP TABLE IF EXISTS attributes;
  CREATE TABLE IF NOT EXISTS attributes (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    name varchar(255) COMMENT 'attribute name',
    did int(10) unsigned COMMENT 'Reference to domains.id',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY did (did),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for attributes' ;

 */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_attribute(Attribute $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('attributes', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: attribute #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setDid($result->did);
      $object->setName($result->name);
      $object->setRevision($result->revision);

      # domain
      $domain = new Domain($object->getDid());
      if ($domain) {
        $object->domain = $domain;
      }
      else {
        drupal_set_message("Inconsistent data: domain #" . $object->getDid() . " in attribute #" . $object->getId());
        $object->setId(0);
      }
    }
  }

  /**
   * @return array
   */
  public static function list_attribute() {
    $options = array();

    $results = db_query("SELECT * FROM attributes");
    foreach ($results as $result) {
      $options[$result->id] = $result->name;
    }

    return $options;
  }

  /*

  DROP TABLE IF EXISTS entities_attributes;
  CREATE TABLE IF NOT EXISTS entities_attributes (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    aid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The attribute id.',
    oid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The object id.',
    required int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'If this attribute required.',
    multiple int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'If this attribute accepts multiple values.',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY aid (aid),
    KEY oid (oid),
    KEY required (required),
    KEY multiple (multiple),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology entities attributes.' ;

 */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_entity_attribute(EntityAttribute $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('entities_attributes', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: entity attribute link #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setAid($result->aid);
      $object->setOid($result->oid);
      $object->setRequired($result->required);
      $object->setMultiple($result->multiple);
      $object->setRevision($result->revision);

      # attribute
      $attribute = new Attribute($object->getAid());
      if ($attribute) {
        $object->attribute = $attribute;
      }
      else {
        drupal_set_message("Inconsistent data: attribute #" . $object->getAid() . " in entity attribute link #" . $object->getId());
        $object->setId(0);
      }

      # object
      $list = Object::getList('Entity');
      if (!isset($list[$object->getOid()])) {
        drupal_set_message("Inconsistent data: entity #" . $object->getOid() . " in entity attribute link #" . $object->getId());
        $object->setId(0);
      }
    }
  }

  /*

  DROP TABLE IF EXISTS relations_attributes;
  CREATE TABLE IF NOT EXISTS relations_attributes (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    aid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The attribute id.',
    oid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The object id.',
    required int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'If this attribute required.',
    multiple int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'If this attribute accepts multiple values.',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY aid (aid),
    KEY oid (oid),
    KEY required (required),
    KEY multiple (multiple),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology relations attributes.' ;

 */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_relation_attribute(RelationAttribute $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('relations_attributes', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: relation attribute link #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setAid($result->aid);
      $object->setOid($result->oid);
      $object->setRequired($result->required);
      $object->setMultiple($result->multiple);
      $object->setRevision($result->revision);

      # attribute
      $attribute = new Attribute($object->getAid());
      if ($attribute) {
        $object->attribute = $attribute;
      }
      else {
        drupal_set_message("Inconsistent data: attribute #" . $object->getAid() . " in relation attribute link #" . $object->getId());
        $object->setId(0);
      }

      # object
      $list = Object::getList('Relation');
      if (!isset($list[$object->getOid()])) {
        drupal_set_message("Inconsistent data: relation #" . $object->getOid() . " in relation attribute link #" . $object->getId());
        $object->setId(0);
      }
    }
  }

  /**
   * @return array
   */
  public static function list_entity_attribute() {
    $options = array();

    $results = db_query("SELECT * FROM entities_attributes");
    foreach ($results as $result) {
      $object = new EntityAttribute();
      $object->setId($result->id);
      $options[$result->id] = $object->getLabel();
    }

    return $options;
  }

  /**
   * @return array
   */
  public static function list_relation_attribute() {
    $options = array();

    $results = db_query("SELECT * FROM relations_attributes");
    foreach ($results as $result) {
      $object = new EntityAttribute();
      $object->setId($result->id);
      $options[$result->id] = $object->getLabel();
    }

    return $options;
  }

  /*

  DROP TABLE IF EXISTS entities;
  CREATE TABLE IF NOT EXISTS entities (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology entities.' ;

 */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_entity(Entity $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('entities', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: entity #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setRevision($result->revision);

      # attributes
      $object->attributes = array();
      $attributes         = self::tablerows('entities_attributes', array('oid' => $object->getId()));
      foreach ($attributes as $attribute) {
        $object->attributes[] = new EntityAttribute($attribute->id);
        db_set_active('inir');
      }

      # relations
      $object->relations = array();
      $relations         = self::tablerows('relations', array('eid1' => $object->getId()));
      foreach ($relations as $relation) {
        $object->relations[$relation->id] = new Relation($relation->id);
        db_set_active('inir');
      }
      $relations = self::tablerows('relations', array('eid2' => $object->getId()));
      foreach ($relations as $relation) {
        $object->relations[$relation->id] = new Relation($relation->id);
        db_set_active('inir');
      }
    }
  }

  /**
   * @return array
   */
  public static function list_entity() {
    $options = array();

    $results = db_query("SELECT * FROM entities");
    foreach ($results as $result) {
      $entity = new Entity();
      $entity->setId($result->id);
      $options[$result->id] = $entity->getLabel();
    }

    return $options;
  }

  /*

  DROP TABLE IF EXISTS relations;
  CREATE TABLE IF NOT EXISTS relations (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    eid1 int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The from ontology entity ID.',
    eid2 int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The to ontology entity ID.',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY eid1 (eid1),
    KEY eid2 (eid2),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology relations.' ;

  DROP TABLE IF EXISTS relations_settings;
  CREATE TABLE IF NOT EXISTS relations_settings (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    rid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'relation.id.',
    settings varchar(255) NOT NULL DEFAULT '' COMMENT 'Settings.',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY rid (rid),
    KEY settings (settings),
    KEY revision (revision)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology relations settings.';

  */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_relation(Relation $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('relations', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: relation #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setEid1($result->eid1);
      $object->setEid2($result->eid2);
      $object->setRevision($result->revision);

      # settings
      $settings = array();
      $list     = self::tablerows('relations_settings', array('rid' => $object->getId()));
      foreach ($list as $item) {
        $settings[] = $item->settings;
      }
      $object->setSettings($settings);

      # attributes
      $object->attributes = array();
      $attributes         = self::tablerows('relations_attributes', array('oid' => $object->getId()));
      foreach ($attributes as $attribute) {
        $object->attributes[] = new RelationAttribute($attribute->id);
        db_set_active('inir');
      }

      # objects
      # object
      $list = Object::getList('Entity');
      if (!isset($list[$object->getEid1()])) {
        drupal_set_message("Inconsistent data: entity #" . $object->getEid1() . " in relation #" . $object->getId());
        $object->setId(0);
      }
      if (!isset($list[$object->getEid2()])) {
        drupal_set_message("Inconsistent data: entity #" . $object->getEid2() . " in relation #" . $object->getId());
        $object->setId(0);
      }
    }
  }

  /**
   * @return array
   */
  public static function list_relation() {
    $options = array();

    $results = db_query("SELECT * FROM relations");
    foreach ($results as $result) {
      $relation = new Relation();
      $relation->setId($result->id);
      $options[$result->id] = $relation->getLabel();
    }

    return $options;
  }


  /*

  DROP TABLE IF EXISTS instances_attributes;
  CREATE TABLE IF NOT EXISTS `instances_attributes` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    `oid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The instance id.',
    `oaid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ontology entity attribute id.',
    `weigth` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Weight.',
    `value` longblob COMMENT 'Serialized array of settings.',
    `revision` int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (`id`),
    KEY `oid` (`oid`),
    KEY `oaid` (`oaid`),
    KEY `revision` (`revision`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances attributes.' ;

  */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_instance_attribute(InstanceAttribute $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('instances_attributes', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: instance attribute #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setOid($result->oid);
      $object->setOaid($result->oaid);
      $object->setWeight($result->weigth);
      $object->setValue($result->value);
      $object->setRevision($result->revision);

      # instance
      $list = Object::getList('Instance');
      if (!isset($list[$object->getOid()])) {
        drupal_set_message("Inconsistent data: instance #" . $object->getOid() . " in instance attribute #" . $object->getId());
        $object->setId(0);
      }

      # entity attribute link
      $list = Object::getList('EntityAttribute');
      if (!isset($list[$object->getOaid()])) {
        drupal_set_message("Inconsistent data: entity attribute link #" . $object->getOaid() . " in instance attribute #" . $object->getId());
        $object->setId(0);
      }
    }
  }

  /*

  DROP TABLE IF EXISTS instances;
  CREATE TABLE IF NOT EXISTS instances (
    id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    eid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ontology entity ID.',
    revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (id),
    KEY eid (eid),
    KEY revision (revision)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances.' ;

  */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_instance(Instance $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('instances', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: instance #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setEid($result->eid);
      $object->setRevision($result->revision);

      # entity
      $list = Object::getList('Entity');
      db_set_active('inir');
      if (!isset($list[$object->getEid()])) {
        drupal_set_message("Inconsistent data: entity #" . $object->getEid() . " in instance #" . $object->getId());
        $object->setId(0);
      }

      # attributes
      $object->attributes = array();
      $attributes         = self::tablerows('instances_attributes', array('oid' => $object->getId()));
      foreach ($attributes as $attribute) {
        $ia                                   = new InstanceAttribute($attribute->id);
        $object->attributes[$ia->getOaid()][] = $ia;
        db_set_active('inir');
      }

      # relations
      $object->relations = array();
      $relations         = self::tablerows('instances_relations', array('iid1' => $object->getId()));
      foreach ($relations as $relation) {
        $object->relations[$relation->id] = new InstanceRelation($relation->id);
        db_set_active('inir');
      }
      $relations = self::tablerows('instances_relations', array('iid2' => $object->getId()));
      foreach ($relations as $relation) {
        $object->relations[$relation->id] = new InstanceRelation($relation->id);
        db_set_active('inir');
      }
    }
  }

  /**
   * @return array
   */
  public static function list_instance() {
    $options = array();

    $results = db_query("SELECT * FROM instances");
    foreach ($results as $result) {
      $object = new Instance();
      $object->setId($result->id);
      $options[$result->id] = $object->getLabel();
    }

    return $options;
  }

  /*

  DROP TABLE IF EXISTS instances_relations_attributes;
  CREATE TABLE IF NOT EXISTS `instances_relations_attributes` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    `oid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The instance relation id.',
    `oaid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ontology relation attribute id.',
    `weight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Weight.',
    `value` longblob COMMENT 'Serialized array of settings.',
    `revision` int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (`id`),
    KEY `oid` (`oid`),
    KEY `oaid` (`oaid`),
    KEY `revision` (`revision`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances attributes.' ;

  */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_instance_relation_attribute(InstanceRelationAttribute $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('instances_relations_attributes', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: instance relation attribute #" . $id);
        return;
      }

      # set
      $object->setId($result->id);
      $object->setOid($result->oid);
      $object->setOaid($result->oaid);
      $object->setWeight($result->weight);
      $object->setValue($result->value);
      $object->setRevision($result->revision);

      # instance
      $list = Object::getList('InstanceRelation');
      if (!isset($list[$object->getOid()])) {
        drupal_set_message("Inconsistent data: instance relation #" . $object->getOid() . " in instance relation attribute #" . $object->getId());
        $object->setId(0);
      }

      # entity attribute link
      $list = Object::getList('RelationAttribute');
      if (!isset($list[$object->getOaid()])) {
        drupal_set_message("Inconsistent data: relation attribute link #" . $object->getOaid() . " in instance relation attribute #" . $object->getId());
        $object->setId(0);
      }
    }
  }

  /*

  DROP TABLE IF EXISTS instances_relations;
  CREATE TABLE IF NOT EXISTS `instances_relations` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
    `rid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relation id.',
    `iid1` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ’from’ ontology instance ID.',
    `iid2` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ’to’ ontology instance ID.',
    `revision` int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
    PRIMARY KEY (`id`),
    KEY `rid` (`rid`),
    KEY `iid1` (`iid1`),
    KEY `iid2` (`iid2`),
    KEY `revision` (`revision`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances relations.';

  */

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_instance_relation(InstanceRelation $object, $id = FALSE) {
    if (FALSE !== $id) {
      $id = intval($id);

      $result = self::tablerow('instances_relations', $id);

      # check
      if (!$result) {
        drupal_set_message("Not found: instance relation #" . $id);
        return;
      }

      # set
      # set
      $object->setId($result->id);
      $object->setRid($result->rid);
      $object->setIid1($result->iid1);
      $object->setIid2($result->iid2);
      $object->setRevision($result->revision);

      # instances
      $list = Object::getList('Instance');
      if (!isset($list[$object->getIid1()])) {
        drupal_set_message("Inconsistent data: instance #" . $object->getIid1() . " in instance relation #" . $object->getId());
        $object->setId(0);
      }
      if (!isset($list[$object->getIid2()])) {
        drupal_set_message("Inconsistent data: instance #" . $object->getIid2() . " in instance relation #" . $object->getId());
        $object->setId(0);
      }

      # entity relation
      $list = Object::getList('Relation');
      if (!isset($list[$object->getRid()])) {
        drupal_set_message("Inconsistent data: relation #" . $object->getRid() . " in instance attribute link #" . $object->getId());
        $object->setId(0);
      }

      # attributes
      db_set_active('inir');
      $object->attributes = array();
      $attributes         = self::tablerows('instances_relations_attributes', array('oid' => $object->getId()));
      foreach ($attributes as $attribute) {
        $object->attributes[] = new InstanceRelationAttribute($attribute->id);
        db_set_active('inir');
      }
    }
  }

  /**
   * @param $object
   * @param bool $id
   */
  public static function load_revision(Revision $object, $id = FALSE) {
    if (FALSE !== $id) {
      global $user;
      $result = self::tablerows('revisions', array('revision' => $id, 'uid' => $user->uid));

      #
      if ($result && ($revision = reset($result))) {
        $object->setDate($revision->timestamp);
        $object->setUid($revision->uid);
        $object->setStatus($revision->status);
        $object->setMeta(($meta = unserialize($revision->meta)) ? $meta : array());
      }
      else {
        $object->setDate(time());
        $object->setMeta(array());
        $object->setUid($user->uid);
        $object->setStatus(REVISION_STATUS_TEMP);

        self::save($object);
      }
    }
  }

  /**
   * Make a request to table
   *
   * @param $table
   * @param $id
   *
   * @return array|bool
   */
  public static function tablerow($table, $id) {
    $result = db_query("SELECT * FROM {$table} WHERE id = :id", array(':id' => $id));
    if ($row = $result->fetch()) {
      return $row;
    }

    return FALSE;
  }

  /**
   * @param $table
   * @param $criteria
   */
  public static function tablerows($table, $criteria = array()) {
    # prepare query
    $query  = "SELECT * FROM {$table} WHERE 1";
    $params = array();
    foreach ($criteria as $key => $value) {
      $query .= " AND $key = :$key";
      $params[":$key"] = $value;
    }

    # execute query
    $list   = array();
    $result = db_query($query, $params);
    while ($item = $result->fetch()) {
      $list[] = $item;
    }

    return $list;
  }

  /**
   * @return array
   */
  public function getLabel(Object $object, $language = 'ru') {
    static $cache;

    //
    if (is_null($cache)) {
      db_set_active('inir');

      $cache = array();

      $labels = self::tablerows('labels');
      foreach ($labels as $label) {
        $cache[$label->otype][$label->oid][$label->lang] = $label->name;
      }

      db_set_active();
    }

    //
    $class = get_class($object);
    $id    = $object->getId();

    //
    if (empty($id)) {
      return '';
    }

    //
    $otype = 'object';
    switch ($class) {
      case 'Domain' :
        $otype = 'domain';
        break;
      case 'Attribute' :
        $otype = 'attribute';
        break;
      case 'Entity' :
        $otype = 'entity';
        break;
      case 'Relation' :
        $otype = 'relation';
        break;
      case 'Instances' :
        $otype = 'instance';
        break;
    }

    if (isset($cache[$otype][$id])) {
      $labels = $cache[$otype][$id];
      if (is_null($language)) {
        return $labels;
      }
      if (!empty($labels[$language])) {
        return $labels[$language];
      }
    }

    $object_types = kbe_object_types();

    return @$object_types[$otype]['name'] . ' #' . $id;
  }
} 