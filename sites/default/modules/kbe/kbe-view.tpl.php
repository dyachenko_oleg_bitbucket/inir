<?php
print theme('table', array(
  'header' => array(
    l('Back to Home', KBE),
    l('Back', KBE . '/' . $object_type),
  ),
  'attributes' => array('style' => 'width:200px'),
));
?>
<?php
$filepath = __DIR__ . '/' . str_replace('.tpl', '--' . $object_type . '.tpl', basename(__FILE__));
if (file_exists($filepath)) {
  include_once $filepath;
  return;
}

// todo remove
var_dump($object);

