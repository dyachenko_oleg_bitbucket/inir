<?php

function kbe_attribute_form($form, &$form_state, $attribute) {

  $form = array(
    '#tree' => TRUE,
  );

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $attribute ? $attribute->getId() : 0,
  );

  $form['name'] = array(
    '#type'  => 'textfield',
    '#title' => 'Name',
    '#value' => $attribute->getName(),
  );

  $form['did'] = array(
    '#type'    => 'select',
    '#title'   => t('Domain'),
    '#options' => Object::getList('Domain'),
    '#value'   => $attribute ? $attribute->getDid() : 0,
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function kbe_attribute_form_submit($form, &$form_state) {
  $values = $form_state['input'];
  $id     = intval($values['id']);
  $id = $id ? $id : FALSE;
  $object = new Attribute($id);
  $object->save($values);
}