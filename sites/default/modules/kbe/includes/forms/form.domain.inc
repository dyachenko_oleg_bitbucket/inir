<?php

/**
 * @param $form
 * @param $form_state
 * @param $domain
 *
 * @return array
 */
function kbe_domain_form($form, &$form_state, Domain $domain) {

  $form = array(
    '#tree' => TRUE,
  );

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $domain ? $domain->getId() : 0,
  );

  $form['name'] = array(
    '#type'  => 'textfield',
    '#title' => 'Name',
    '#value' => $domain->getName(),
  );

  $type = $domain ? $domain->getType() : 'string';
  $form['type'] = array(
    '#type'    => 'select',
    '#title'   => t('Type'),
    '#options' => array(
      'bool'   => t('Boolean'),
      'int'    => t('Integer'),
      'float'  => t('Float'),
      'string' => t('String'),
      'date'   => t('Date'),
      'enum'   => t('Enum'),
    ),
    '#value'   => $type,
    '#ajax'    => array(
      'event'    => 'change',
      'callback' => 'kbe_domain_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  $form['settings'] = array(
    '#type'   => 'fieldset',
    '#title'  => 'enum' == $type ? t('Values') : t('Settings'),
    '#prefix' => '<div id="settings-div">',
    '#suffix' => '</div>',
  );

  //
  $settings = array();
  $settings_keys = array();
  $settings_vals = array();

  // save just in case
  $clone = clone $domain;

  // async
  $domain->applyChanges();

  // init values
  if ($domain) {
    if ($settings = $domain->getSettings()) {
      $settings_keys = array_keys($settings);
      $settings_vals = array_values($settings);
      if (empty($form_state['settings_count'])) {
        $form_state['settings_count'] = count($settings);
      }
    }
  }

  if (empty($form_state['settings_count'])) {
    $form_state['settings_count'] = 1;
  }

  //
  for ($i = 0; $i < $form_state['settings_count']; $i++) {
    $form['settings']['option'][$i] = array(
      '#type' => 'textfield',
      '#value' => '',
    );
    $form['settings']['index'][$i] = array(
      '#type' => 'hidden',
      '#value' => 0,
    );

    if (isset($settings_keys[$i])) {
      $form['settings']['option'][$i]['#value'] = $settings_vals[$i];
      $form['settings']['index'][$i]['#value'] = $settings_keys[$i];
    }
  }
  $form['settings']['more'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add more'),
    '#submit' => array('kbe_domain_settings_more'),
    '#ajax'   => array(
      'callback' => 'kbe_domain_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  if (FALSE && ($form_state['settings_count'] > 1)) {
    $form['settings']['remove'] = array(
      '#type'   => 'submit',
      '#value'  => t('Remove one'),
      '#submit' => array('kbe_domain_settings_none'),
      '#ajax'   => array(
        'callback' => 'kbe_domain_settings_ajax',
        'wrapper'  => 'settings-div',
      ),
    );
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  $domain->processed = TRUE;

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function kbe_domain_settings_ajax(&$form, $form_state) {
  $type = $form_state['input']['type'];
  $form['settings']['#title'] = 'enum' == $type ? 'Values' : 'Settings';
  return $form['settings'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function kbe_domain_settings_more($form, &$form_state) {
  $form_state['settings_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function kbe_domain_settings_none($form, &$form_state) {
  $form_state['settings_count']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function kbe_domain_form_validate($form, &$form_state) {
  // todo domain validation
}

/**
 * @param $form
 * @param $form_state
 */
function kbe_domain_form_submit($form, &$form_state) {
  $values = $form_state['input'];
  $id = intval($values['id']);
  $id = $id ? $id : FALSE;
  $object = new Domain($id);
  $object->save($values);
}