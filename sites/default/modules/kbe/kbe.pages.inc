<?php

/**
 * @param null $object_type
 * @param null $object_id
 * @param string $action
 */
function kbe_callback($object_type = NULL, $object_id = NULL, $action = 'view') {
  // main page
  if (empty($object_type)) {
    return theme('kbe_main');
  }

  $object_types = kbe_object_types();
  if (!isset($object_types[$object_type])) {
    drupal_set_message("<em>$object_type</em> code is not supported");
    drupal_goto(KBE);
  }

  // list
  if (empty($object_id)) {
    return theme('kbe_list', array('object_type' => $object_type));
  }

  // action
  if ('new' == $object_id) {
    $action = 'new';
    $object_id = FALSE;
  }

  // object
  $class = kbe_classes('default', $object_type);
  $object = new $class($object_id);
  switch ($action) {
    case 'view' :
      if (!$object->getId()) {
        drupal_not_found();
      }
      return theme('kbe_item', array('object_type' => $object_type, 'object' => $object));
    case 'edit' :
      if (!$object->getId()) {
        drupal_not_found();
      }
    case 'new' :
      $form_id = sprintf('kbe_%s_form', $object_type);
      if (function_exists($form_id)) {
        $form = drupal_get_form($form_id, $object);
        return theme('table', array(
          'header'     => array(
            l('Back to Home', KBE),
            l('Back', KBE . '/' . $object_type),
          ),
          'attributes' => array('style' => 'width:200px'),
        )) . drupal_render($form);
      }
      else {
        drupal_not_found();
      }
  }

  drupal_not_found();
}