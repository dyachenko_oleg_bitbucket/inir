<?php

/**
 * @return array
 */
function woe_objects() {
  return array(
    'entity',
    'relation',
    'instance',
    'attribute',
    'domain',
  );
}

/**
 * @return string
 */
function woe_theme_main() {
  $objects = woe_objects();

  $rows = array();
  foreach ($objects as $object) {
    $rows[] = array(
      ucfirst($object),
      l('Go to list', WOE . '/' . $object),
    );
  }

  return
    theme('table', array(
      'header' => array(
        'Name',
        'Action'
      ),
      'rows'   => $rows,
    ));
}

/**
 * @param $object
 *
 * @return string
 */
function woe_theme_list($object) {
  $output = theme('table', array(
    'header'     => array(
      l('Back to Home', WOE),
      l('New', WOE . '/' . $object . '/new'),
    ),
  ));

  // load
  $driver = 'mysql';
  if ($class = woe_classes($object, $driver)) {
    //
    $list = $class::getList();

    //
    $rows = array();
    foreach ($list as $id => $label) {
      $rows[] = array(
        $label,
        l('view', WOE . '/' . $object . '/' . $id . '/view'),
        l('edit', WOE . '/' . $object . '/' . $id . '/edit'),
      );
    }
    $output .= theme('table', array(
      'header' => array(
        'Label',
        'View',
        'Edit'
      ),
      'rows'   => $rows,
    ));
  }

  return $output;
}
