<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS instances_relations;
CREATE TABLE IF NOT EXISTS `instances_relations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  `rid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relation id.',
  `iid1` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ’from’ ontology instance ID.',
  `iid2` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ’to’ ontology instance ID.',
  `revision` int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (`id`),
  KEY `rid` (`rid`),
  KEY `iid1` (`iid1`),
  KEY `iid2` (`iid2`),
  KEY `revision` (`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances relations.';


 */

class InstanceRelation extends AbstractInstanceRelation implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('instances_relations', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: instance relation #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setRid($result->rid);
    $this->setIid1($result->iid1);
    $this->setIid2($result->iid2);
    $this->setRevision($result->revision);

    # instances
    if (!Cache::load('Instance', $this->getIid1(), TRUE)) {
      drupal_set_message("Inconsistent data: instance #" . $this->getIid1() . " in instance relation #" . $this->getId());
      $this->setId(0);
    }
    if (!Cache::load('Instance', $this->getIid2(), TRUE)) {
      drupal_set_message("Inconsistent data: instance #" . $this->getIid2() . " in instance relation #" . $this->getId());
      $this->setId(0);
    }

    # entity relation
    if (!Cache::load('Relation', $this->getRid(), TRUE)) {
      drupal_set_message("Inconsistent data: relation #" . $this->getRid() . " in instance attribute link #" . $this->getId());
      $this->setId(0);
    }

    # attributes
    $this->attributes = array();
    $attributes = Cache::tablerows(InstanceRelationAttribute::table(), array('oid' => $this->getId()));
    foreach ($attributes as $attribute) {
      $this->attributes[] = Cache::load('InstanceRelationAttribute', $attribute->id);
    }

  }

  /**
   * @return string
   */
  public static function table() {
    return 'instances_relations';
  }
} 