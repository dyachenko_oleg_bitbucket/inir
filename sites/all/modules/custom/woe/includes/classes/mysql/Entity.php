<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS entities;
CREATE TABLE IF NOT EXISTS entities (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY revision (revision)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology entities.' ;


 */

class Entity extends AbstractEntity implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('entities', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: entity #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setRevision($result->revision);

    # attributes
    $this->attributes = array();
    $attributes = Cache::tablerows(EntityAttribute::table(), array('oid' => $this->getId()));
    foreach ($attributes as $attribute) {
      $this->attributes[] = Cache::load('EntityAttribute', $attribute->id);
    }

    # relations
    $this->relations = array();
    $relations = Cache::tablerows(Relation::table(), array('eid1' => $this->getId()));
    foreach ($relations as $relation) {
      $this->relations[$relation->id] = Cache::load('Relation', $relation->id);
    }
    $relations = Cache::tablerows(Relation::table(), array('eid2' => $this->getId()));
    foreach ($relations as $relation) {
      $this->relations[$relation->id] = Cache::load('Relation', $relation->id);
    }
  }

  /**
   * @return string
   */
  public static function table() {
    return 'entities';
  }

  /**
   *
   */
  public static function getList() {
    db_set_active('inir');

    $options = array();

    $results = db_query("SELECT * FROM entities");
    foreach ($results as $result) {
      $options[$result->id] = ($label = Cache::label($result->id, 'entity')) ? $label : __CLASS__ . ' #'.$result->id;
    }

    db_set_active();

    return $options;
  }

  /**
   *
   */
  public function save() {
    db_set_active('inir');

    // save relation
    if (!$this->getId()) {
      $id = db_insert('entities')->fields(array(
        'id'       => NULL,
        'revision' => 0,
      ))->execute();
    }
    else {
      $id = $this->getId();
      db_update('entities')->fields(array(
        'revision' => 0,
      ))->condition('id', $id)->execute();
    }

    if ($this->attributes) {
      foreach ($this->attributes as $ea) {
        $ea->setOid(intval($id));
        $ea->save();
      }
    }

    db_set_active();
  }
}
