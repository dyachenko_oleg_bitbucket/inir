<?php


class Cache {
  /**
   * @var array
   */
  private static $cache = array();

  /**
   * @param      $class
   * @param bool $id
   * @param bool $check_exist
   *
   * @return bool
   */
  public static function load($class, $id = FALSE, $check_exist = FALSE, $reset = FALSE) {
    # reset
    if ($reset) {
      if ($id) {
        self::$cache[$class][$id] = FALSE;
      }
      else {
        unset(self::$cache[$class]);
      }
    }

    # load all domains at the first load
    if (!isset(self::$cache[$class])) {
      self::$cache[$class] = array();

      $table = $class::table();
      $results = self::tablerows($table, array());
      foreach ($results as $result) {
        self::$cache[$class][$result->id] = FALSE;
      }
    }

    #
    if (FALSE !== $id) {
      if (array_key_exists($id, self::$cache[$class])) {
        if ($check_exist) {
          return TRUE;
        }
        if (FALSE === self::$cache[$class][$id]) {
          self::$cache[$class][$id] = new $class($id);
        }
        return self::$cache[$class][$id];
      }
      return FALSE;
    }

    #
    return self::$cache[$class];
  }

  /**
   * Make a request to table
   *
   * @param $table
   * @param $id
   *
   * @return array|bool
   */
  public static function tablerow($table, $id) {
    db_set_active('inir');

    $result = db_query("SELECT * FROM {$table} WHERE id = :id", array(':id' => $id));
    if ($row = $result->fetch()) {
      db_set_active();
      return $row;
    }

    db_set_active();

    return FALSE;
  }

  /**
   * @param $table
   * @param $criteria
   */
  public static function tablerows($table, $criteria = array()) {
    db_set_active('inir');

    # prepare query
    $query = "SELECT * FROM {$table} WHERE 1";
    $params = array();
    foreach ($criteria as $key => $value) {
      $query .= " AND $key = :$key";
      $params[":$key"] = $value;
    }

    # execute query
    $list = array();
    $result = db_query($query, $params);
    while ($item = $result->fetch()) {
      $list[] = $item;
    }

    db_set_active();

    return $list;
  }

  /**
   * @param $oid
   * @param $otype
   */
  public static function label($oid, $otype, $language = 'ru') {
    static $cache;

    //
    if (is_null($cache)) {
      $cache = array();

      $labels = self::tablerows('labels');
      foreach ($labels as $label) {
        $cache[$label->otype][$label->oid][$label->lang] = $label->name;
      }
    }

    //
    if (isset($cache[$otype][$oid])) {
      $labels = $cache[$otype][$oid];
      if (is_null($language)) {
        return $labels;
      }
      if (!empty($labels[$language])) {
        return $labels[$language];
      }
    }

    $object_types = kbe_object_types();

    return $object_types[$otype]['name'] . ' #' . $oid;
  }
} 
