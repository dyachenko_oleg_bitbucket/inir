<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS instances;
CREATE TABLE IF NOT EXISTS instances (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  eid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ontology entity ID.',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY eid (eid),
  KEY revision (revision)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances.' ;


 */

class Instance extends AbstractInstance implements Cacheable {
  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('instances', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: instance #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setEid($result->eid);
    $this->setRevision($result->revision);

    # entity
    if (!Cache::load('Entity', $this->getEid(), TRUE)) {
      drupal_set_message("Inconsistent data: entity #" . $this->getEid() . " in instance #" . $this->getId());
      $this->setId(0);
    }

    # attributes
    $this->attributes = array();
    $attributes = Cache::tablerows(InstanceAttribute::table(), array('oid' => $this->getId()));
    foreach ($attributes as $attribute) {
      $ia = Cache::load('InstanceAttribute', $attribute->id);
      $this->attributes[$ia->getOaid()][] = $ia;
    }

    # relations
    $this->relations = array();
    $relations = Cache::tablerows(InstanceRelation::table(), array('iid1' => $this->getId()));
    foreach ($relations as $relation) {
      $this->relations[$relation->id] = Cache::load('InstanceRelation', $relation->id);
    }
    $relations = Cache::tablerows(InstanceRelation::table(), array('iid2' => $this->getId()));
    foreach ($relations as $relation) {
      $this->relations[$relation->id] = Cache::load('InstanceRelation', $relation->id);
    }
  }

  /**
   * @return string
   */
  public static function table() {
    return 'instances';
  }

  /**
   *
   */
  public static function getList() {
    db_set_active('inir');

    $options = array();

    $results = db_query("SELECT * FROM instances");
    foreach ($results as $result) {
      $options[$result->id] = 'Instance #' . $result->id;
    }

    db_set_active();

    return $options;
  }

  public function save() {
    // todo
  }
} 
