<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS relations;
CREATE TABLE IF NOT EXISTS relations (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  eid1 int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The from ontology entity ID.',
  eid2 int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The to ontology entity ID.',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY eid1 (eid1),
  KEY eid2 (eid2),
  KEY revision (revision)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology relations.' ;

DROP TABLE IF EXISTS relations_settings;
CREATE TABLE IF NOT EXISTS relations_settings (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  rid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'relation.id.',
  settings varchar(255) NOT NULL DEFAULT '' COMMENT 'Settings.',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY rid (rid),
  KEY settings (settings),
  KEY revision (revision)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology relations settings.';

 */

class Relation extends AbstractRelation implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('relations', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: relation #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setEid1($result->eid1);
    $this->setEid2($result->eid2);
    $this->setRevision($result->revision);

    # settings
    $settings = array();
    $list = Cache::tablerows('relations_settings', array('rid' => $this->getId()));
    foreach ($list as $item) {
      $settings[] = $item->settings;
    }
    $this->setSettings($settings);

    # attributes
    $this->attributes = array();
    $attributes = Cache::tablerows(RelationAttribute::table(), array('oid' => $this->getId()));
    foreach ($attributes as $attribute) {
      $this->attributes[] = Cache::load('RelationAttribute', $attribute->id);
    }

    # objects
    if (!Cache::load('Entity', $this->getEid1(), TRUE)) {
      drupal_set_message("Inconsistent data: entity #" . $this->getEid1() . " in relation #" . $this->getId());
      $this->setId(0);
    }
    if (!Cache::load('Entity', $this->getEid2(), TRUE)) {
      drupal_set_message("Inconsistent data: entity #" . $this->getEid2() . " in relation #" . $this->getId());
      $this->setId(0);
    }
  }

  /**
   * @return string
   */
  public static function table() {
    return 'relations';
  }

  /**
   *
   */
  public static function getList() {
    db_set_active('inir');

    $options = array();

    $results = db_query("SELECT * FROM relations");
    foreach ($results as $result) {
      $options[$result->id] = Cache::label($result->id, 'relation');
    }

    db_set_active();

    return $options;
  }

  /**
   *
   */
  public function save() {
    db_set_active('inir');

    // save relation
    if (!$this->getId()) {
      $id = db_insert('relations')->fields(array(
        'id'       => NULL,
        'eid1'     => $this->getEid1(),
        'eid2'     => $this->getEid2(),
        'revision' => 0,
      ))->execute();
    }
    else {
      $id = $this->getId();
      db_update('relations')->fields(array(
        'eid1'     => $this->getEid1(),
        'eid2'     => $this->getEid2(),
        'revision' => 0,
      ))->condition('id', $id)->execute();
    }

    // delete settings
    db_delete('relations_settings')->condition('rid', $id)->execute();

    // insert settings
    if ($this->getSettings()) {
      foreach ($this->getSettings() as $setting) {
        db_insert('relations_settings')->fields(array(
          'id'       => NULL,
          'rid'      => $id,
          'settings' => $setting,
          'revision' => 0,
        ))->execute();
      }
    }

    if ($this->attributes) {
      foreach ($this->attributes as $ra) {
        $ra->setOid(intval($id));
        $ra->save();
      }
    }

    db_set_active();
  }
} 
