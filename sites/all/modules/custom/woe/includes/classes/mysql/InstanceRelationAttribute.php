<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS instances_relations_attributes;
CREATE TABLE IF NOT EXISTS `instances_relations_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  `oid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The instance relation id.',
  `oaid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ontology relation attribute id.',
  `weight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Weight.',
  `value` longblob COMMENT 'Serialized array of settings.',
  `revision` int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (`id`),
  KEY `oid` (`oid`),
  KEY `oaid` (`oaid`),
  KEY `revision` (`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances attributes.' ;


 */

class InstanceRelationAttribute extends AbstractInstanceAttribute implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('instances_relations_attributes', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: instance relation attribute #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setOid($result->oid);
    $this->setOaid($result->oaid);
    $this->setWeight($result->weight);
    $this->setValue($result->value);
    $this->setRevision($result->revision);

    # instance
    if (!Cache::load('InstanceRelation', $this->getOid(), TRUE)) {
      drupal_set_message("Inconsistent data: instance relation #" . $this->getOid() . " in instance relation attribute #" . $this->getId());
      $this->setId(0);
    }

    # entity attribute link
    if (!Cache::load('RelationAttribute', $this->getOaid(), TRUE)) {
      drupal_set_message("Inconsistent data: relation attribute link #" . $this->getOaid() . " in instance relation attribute #" . $this->getId());
      $this->setId(0);
    }
  }

  /**
   * @return string
   */
  public static function table() {
    return 'instances_relations_attributes';
  }
} 