<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS instances_attributes;
CREATE TABLE IF NOT EXISTS `instances_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  `oid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The instance id.',
  `oaid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ontology entity attribute id.',
  `weigth` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Weight.',
  `value` longblob COMMENT 'Serialized array of settings.',
  `revision` int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (`id`),
  KEY `oid` (`oid`),
  KEY `oaid` (`oaid`),
  KEY `revision` (`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The base table for ontology instances attributes.' ;


 */

class InstanceAttribute extends AbstractInstanceAttribute implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('instances_attributes', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: instance attribute #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setOid($result->oid);
    $this->setOaid($result->oaid);
    $this->setWeight($result->weigth);
    $this->setValue($result->value);
    $this->setRevision($result->revision);

    # instance
    if (!Cache::load('Instance', $this->getOid(), TRUE)) {
      drupal_set_message("Inconsistent data: instance #" . $this->getOid() . " in instance attribute #" . $this->getId());
      $this->setId(0);
    }

    # entity attribute link
    if (!Cache::load('EntityAttribute', $this->getOaid(), TRUE)) {
      drupal_set_message("Inconsistent data: entity attribute link #" . $this->getOaid() . " in instance attribute #" . $this->getId());
      $this->setId(0);
    }

  }

  /**
   * @return string
   */
  public static function table() {
    return 'instances_attributes';
  }
} 