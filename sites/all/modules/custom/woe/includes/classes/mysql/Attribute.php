<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS attributes;
CREATE TABLE IF NOT EXISTS attributes (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  name varchar(255) COMMENT 'attribute name',
  did int(10) unsigned COMMENT 'Reference to domains.id',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY did (did),
  KEY revision (revision)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for attributes' ;

 */

class Attribute extends AbstractAttribute implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('attributes', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: attribute #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setDid($result->did);
    $this->setName($result->name);
    $this->setRevision($result->revision);

    # domain
    $domain = Cache::load('Domain', $this->getDid());
    if ($domain) {
      $this->domain = $domain;
    }
    else {
      drupal_set_message("Inconsistent data: domain #" . $this->getDid() . " in attribute #" . $this->getId());
      $this->setId(0);
    }
  }

  /**
   *
   */
  public function save() {
    db_set_active('inir');
    if (!$this->getId()) {
      db_insert('attributes')->fields(array(
        'id'       => NULL,
        'name'     => $this->getName(),
        'did'      => $this->getDid(),
        'revision' => 0,
      ))->execute();
    }
    else {
      $id = $this->getId();
      db_update('attributes')->fields(array(
        'name'     => $this->getName(),
        'did'      => $this->getDid(),
        'revision' => 0,
      ))->condition('id', $id)->execute();
    }
    db_set_active();
  }

  /**
   * @return string
   */
  public static function table() {
    return 'attributes';
  }

  /**
   *
   */
  public static function getList() {
    db_set_active('inir');

    $options = array();

    $results = db_query("SELECT * FROM attributes");
    foreach ($results as $result) {
      $options[$result->id] = $result->name;
    }

    db_set_active();

    return $options;
  }
}