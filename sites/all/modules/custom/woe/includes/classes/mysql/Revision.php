<?php
/**
 * Created by PhpStorm.
 * User: skullhole
 * Date: 16/04/14
 * Time: 11:51
 */

define('REVISION_STATUS_TEMP', 0);
define('REVISION_STATUS_LIVE', 1);
define('REVISION_STATUS_BLOCK', 2);

class Revision extends AbstractRevision {
  /**
   *
   */
  protected function load() {
    db_set_active('inir');

    $result = db_query("SELECT * FROM {revisions} WHERE revision = :revision", array(':revision' => $this->getRevision()));
    if ($row = $result->fetch()) {
      db_set_active();

      $this->setDate($row->timestamp);
      $this->setUid($row->uid);
      $this->setStatus($row->status);
      $this->setMeta(($meta = unserialize($row->meta)) ? $meta : array());
    }
    else {
      global $user;
      $this->setDate(time());
      $this->setMeta(array());
      $this->setUid($user->uid);
      $this->setStatus(REVISION_STATUS_TEMP);

      $this->save();
    }
  }

  /**
   *
   */
  protected function save() {
    db_set_active('inir');

    db_query("REPLACE INTO {revisions} SET revision = :revision, uid = :uid, status = :status, timestamp = :timestamp, meta = :meta", array(
      ':revision'  => $this->getRevision(),
      ':uid'       => $this->getUid(),
      ':status'    => $this->getStatus(),
      ':timestamp' => $this->getDate(),
      ':meta'      => serialize($this->getMeta()),
    ));

    db_set_active();
  }
} 
