<?php

/**
 * MySQL
 */
class RelationAttribute extends AbstractAttachedAttribute implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('relations_attributes', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: relation attribute link #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setAid($result->aid);
    $this->setOid($result->oid);
    $this->setRequired($result->required);
    $this->setMultiple($result->multiple);
    $this->setRevision($result->revision);

    # attribute
    $attribute = Cache::load('Attribute', $this->getAid());
    if ($attribute) {
      $this->attribute = $attribute;
    }
    else {
      drupal_set_message("Inconsistent data: attribute #" . $this->getAid() . " in relation attribute link #" . $this->getId());
      $this->setId(0);
    }

    # object
    if (!Cache::load('Entity', $this->getOid(), TRUE)) {
      drupal_set_message("Inconsistent data: relation #" . $this->getOid() . " in relation attribute link #" . $this->getId());
      $this->setId(0);
    }
  }

  /**
   * @return string
   */
  public static function table() {
    return 'relations_attributes';
  }

  /**
   *
   */
  public function save() {
    db_set_active('inir');

    // save relation
    if (!$this->getId()) {
      $id = db_insert('relations_attributes')->fields(array(
        'id'       => NULL,
        'aid'      => $this->getAid(),
        'oid'      => $this->getOid(),
        'required' => $this->getRequired(),
        'multiple' => $this->getMultiple(),
        'revision' => 0,
      ))->execute();
    }
    else {
      $id = $this->getId();
      db_update('relations_attributes')->fields(array(
        'aid'      => $this->getAid(),
        'oid'      => $this->getOid(),
        'required' => $this->getRequired(),
        'multiple' => $this->getMultiple(),
        'revision' => 0,
      ))->condition('id', $id)->execute();
    }

    db_set_active();
  }
} 