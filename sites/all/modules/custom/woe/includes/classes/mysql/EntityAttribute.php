<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS entities_attributes;
CREATE TABLE IF NOT EXISTS entities_attributes (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  aid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The attribute id.',
  oid int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The object id.',
  required int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'If this attribute required.',
  multiple int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'If this attribute accepts multiple values.',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY aid (aid),
  KEY oid (oid),
  KEY required (required),
  KEY multiple (multiple),
  KEY revision (revision)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for ontology entities attributes.' ;

 */

class EntityAttribute extends AbstractAttachedAttribute implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('entities_attributes', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: entity attribute link #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setAid($result->aid);
    $this->setOid($result->oid);
    $this->setRequired($result->required);
    $this->setMultiple($result->multiple);
    $this->setRevision($result->revision);

    # attribute
    $attribute = Cache::load('Attribute', $this->getAid());
    if ($attribute) {
      $this->attribute = $attribute;
    }
    else {
      drupal_set_message("Inconsistent data: attribute #" . $this->getAid() . " in entity attribute link #" . $this->getId());
      $this->setId(0);
    }

    # object
    if (!Cache::load('Entity', $this->getOid(), TRUE)) {
      drupal_set_message("Inconsistent data: entity #" . $this->getOid() . " in entity attribute link #" . $this->getId());
      $this->setId(0);
    }
  }

  /**
   * @return string
   */
  public static function table() {
    return 'entities_attributes';
  }

  /**
   *
   */
  public function save() {
    db_set_active('inir');

    // save relation
    if (!$this->getId()) {
      $id = db_insert('entities_attributes')->fields(array(
        'id'       => NULL,
        'aid'      => $this->getAid(),
        'oid'      => $this->getOid(),
        'required' => $this->getRequired(),
        'multiple' => $this->getMultiple(),
        'revision' => 0,
      ))->execute();
    }
    else {
      $id = $this->getId();
      db_update('entities_attributes')->fields(array(
        'aid'      => $this->getAid(),
        'oid'      => $this->getOid(),
        'required' => $this->getRequired(),
        'multiple' => $this->getMultiple(),
        'revision' => 0,
      ))->condition('id', $id)->execute();
    }

    db_set_active();
  }
} 