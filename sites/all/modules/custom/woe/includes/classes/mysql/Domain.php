<?php

/**
 * MySQL
 */
/*

DROP TABLE IF EXISTS domains;
CREATE TABLE IF NOT EXISTS domains (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  name varchar(255) NOT NULL DEFAULT '' COMMENT 'Name.',
  type varchar(255) NOT NULL DEFAULT '' COMMENT 'Must be one of the following: bool, int, float, string, enum.',
  settings longblob COMMENT 'Serialized array of settings.',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY type (type),
  KEY revision (revision)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for domains' ;

DROP TABLE IF EXISTS domains_enum;
CREATE TABLE IF NOT EXISTS domains_enum (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier.',
  did int(10) unsigned COMMENT 'Reference to domains.id',
  weight int(10) unsigned COMMENT 'weight value',
  value TEXT COMMENT 'Enum value',
  revision int(10) unsigned DEFAULT NULL COMMENT 'The current revision identifier.',
  PRIMARY KEY (id),
  KEY did (did),
  KEY revision (revision)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='The base table for domain enums' ;

 */

/**
 * Class Domain
 */
class Domain extends AbstractDomain implements Cacheable {

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $result = Cache::tablerow('domains', $id);

    # check
    if (!$result) {
      drupal_set_message("Not found: domain #" . $id);
      return;
    }

    # set
    $this->setId($result->id);
    $this->setType($result->type);
    $this->setName($result->name);
    if ($result->settings && ($settings = @unserialize($result->settings)) && is_array($settings)) {
      $this->setSettings($settings);
    }
    $this->setRevision($result->revision);

    # enum
    if ('enum' == $this->getType()) {
      $values = array();
      $list = Cache::tablerows('domains_enum', array('did' => $this->getId()));
      foreach ($list as $item) {
        $values[$item->id] = $item->value;
      }
      $this->setValues($values);
    }
  }

  /**
   *
   */
  public function save() {
    db_set_active('inir');

    if (!$this->getId()) {
      $id = db_insert('domains')->fields(array(
        'id'       => NULL,
        'name'     => $this->getName(),
        'type'     => $this->getType(),
        'settings' => serialize($this->getSettings()),
        'revision' => 0,
      ))->execute();
      $this->setId($id);
    }
    else {
      $id = $this->getId();
      db_update('domains')->fields(array(
        'name'     => $this->getName(),
        'type'     => $this->getType(),
        'settings' => serialize($this->getSettings()),
        'revision' => 0,
      ))->condition('id', $id)->execute();
    }

    if ('enum' == $this->getType()) {
      foreach ($this->getValues() as $weight => $value) {
        // todo enum values update
        db_insert('domains_enum')->fields(array(
          'id'       => NULL,
          'did'      => $id,
          'weight'   => $weight,
          'value'    => $value,
          'revision' => 0,
        ))->execute();
      }
    }

    db_set_active();
  }

  /**
   * @param array $values
   */
  public function setValues($values) {
    $this->values = $values;
  }

  /**
   * @return array
   */
  public function getValues() {
    return $this->values;
  }

  /**
   * @var array
   */
  private $values = array();

  /**
   * @return string
   */
  public static function table() {
    return 'domains';
  }

  /**
   * @return string
   */
  public function toString() {
    return 'Domain #' . $this->getId() . ' of type ' . $this->getType();
  }

  /**
   *
   */
  public static function getList() {
    db_set_active('inir');

    $options = array();

    $results = db_query("SELECT * FROM domains");
    foreach ($results as $result) {
      $options[$result->id] = $result->name;
    }

    db_set_active();

    return $options;
  }
}
