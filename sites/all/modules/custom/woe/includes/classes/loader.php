<?php

/**
 * @param null   $class
 * @param string $driver
 *
 * @return array|null
 */
function woe_classes($class = NULL, $driver = 'mysql') {
  $classes = array(
    'abstract' => array(
      'object'             => 'Object',
      'domain'             => 'AbstractDomain',
      'attribute'          => 'AbstractAttribute',
      'attribute_attached' => 'AbstractAttachedAttribute',
      'entity'             => 'AbstractEntity',
      'relation'           => 'AbstractRelation',
      'instance_attribute' => 'AbstractInstanceAttribute',
      'instance'           => 'AbstractInstance',
      'instance_relation'  => 'AbstractInstanceRelation',
      'revision'           => 'AbstractRevision',
    ),
    'mysql'    => array(
      'cacheable'                   => 'Cacheable',
      'cache'                       => 'Cache',
      'domain'                      => 'Domain',
      'attribute'                   => 'Attribute',
      'attribute_entity'            => 'EntityAttribute',
      'attribute_relation'          => 'RelationAttribute',
      'entity'                      => 'Entity',
      'relation'                    => 'Relation',
      'instance_attribute'          => 'InstanceAttribute',
      'instance'                    => 'Instance',
      'instance_relation'           => 'InstanceRelation',
      'instance_relation_attribute' => 'InstanceRelationAttribute',
      'revision'                    => 'Revision',
      'revision_changes'            => 'RevisionChanges',
    ),
  );

  //
  if (is_null($class)) {
    return $classes;
  }

  //
  if (isset($classes[$driver][$class])) {
    return $classes[$driver][$class];
  }

  return NULL;
}

/**
 *
 */
foreach (woe_classes() as $subdirectory => $classNames) {
  foreach ($classNames as $className) {
    require_once __DIR__ . '/' . $subdirectory . '/' . $className . '.php';
  }
}
