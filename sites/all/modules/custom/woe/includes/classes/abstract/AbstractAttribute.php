<?php


abstract class AbstractAttribute extends Object {

  /**
   * @var string
   */
  protected $name = '';

  /**
   * @var int
   */
  protected $did = 0;

  /**
   * @param int $did
   */
  public function setDid($did) {
    $this->did = $did;
  }

  /**
   * @return int
   */
  public function getDid() {
    return $this->did;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   *
   */
  public function toString() {
    return $this->getName();
  }
} 