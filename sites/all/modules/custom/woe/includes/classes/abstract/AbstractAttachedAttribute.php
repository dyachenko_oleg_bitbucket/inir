<?php


abstract class AbstractAttachedAttribute extends Object {
  /**
   * @var int
   */
  protected $aid = 0;

  /**
   * @var int
   */
  protected $oid = 0;

  /**
   * @var bool
   */
  protected $multiple = FALSE;

  /**
   * @var bool
   */
  protected $required = FALSE;

  /**
   * @param int $aid
   */
  public function setAid($aid) {
    $this->aid = $aid;
  }

  /**
   * @return int
   */
  public function getAid() {
    return $this->aid;
  }

  /**
   * @param boolean $multiple
   */
  public function setMultiple($multiple) {
    $this->multiple = $multiple;
  }

  /**
   * @return boolean
   */
  public function getMultiple() {
    return $this->multiple;
  }

  /**
   * @param int $oid
   */
  public function setOid($oid) {
    $this->oid = $oid;
  }

  /**
   * @return int
   */
  public function getOid() {
    return $this->oid;
  }

  /**
   * @param boolean $required
   */
  public function setRequired($required) {
    $this->required = $required;
  }

  /**
   * @return boolean
   */
  public function getRequired() {
    return $this->required;
  }
}