<?php


abstract class AbstractRelation extends Object {

  /**
   * @var int
   */
  protected $eid1 = 0;

  /**
   * @param int $eid1
   */
  public function setEid1($eid1) {
    $this->eid1 = $eid1;
  }

  /**
   * @return int
   */
  public function getEid1() {
    return $this->eid1;
  }

  /**
   * @param int $eid2
   */
  public function setEid2($eid2) {
    $this->eid2 = $eid2;
  }

  /**
   * @return int
   */
  public function getEid2() {
    return $this->eid2;
  }

  /**
   * @var int
   */
  protected $eid2 = 0;

  /**
   * @param array $settings
   */
  public function setSettings($settings) {
    $this->settings = $settings;
  }

  /**
   * @return array
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * @var array
   */
  protected $settings = array();

  /**
   * @return array
   */
  public static function availableSettings() {
    return array(
      'bipolar'     => 'Bipolar',
      'reflexive'   => 'Reflexive',
      'irreflexive' => 'Irreflexive',
    );
  }
}