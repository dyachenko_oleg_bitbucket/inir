<?php


abstract class AbstractDomain extends Object {
  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param array $settings
   */
  public function setSettings($settings) {
    $this->settings = $settings;
  }

  /**
   * @return array
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * Type
   *
   * Is one of 'int', 'string', 'enum', 'float', 'bool'
   *
   * @var string
   */
  protected $type = 'int';

  /**
   * Settings
   *
   * To store limitations or other rules
   *
   * @var array
   */
  protected $settings = array();

  /**
   * @var string
   */
  protected $name = '';

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }


  /**
   * todo Validate value
   *
   * @param $value
   *
   * @return bool
   */
  public function validate($value) {
    return FALSE;
  }
} 