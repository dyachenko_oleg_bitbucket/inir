<?php

abstract class AbstractInstance extends Object {
  /**
   * @param int $eid
   */
  public function setEid($eid) {
    $this->eid = $eid;
  }

  /**
   * @return int
   */
  public function getEid() {
    return $this->eid;
  }

  /**
   * @var int
   */
  protected $eid = 0;
} 