<?php

/**
 * Class Revision
 */
abstract class Object {

  /**
   * ID
   *
   * @var string
   */
  protected $id = NULL;

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  protected $label = NULL;

  /**
   * @param null $label
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * @return null
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Revision number
   *
   * @var string
   */
  protected $revision = NULL;

  /**
   * @param string $revision
   */
  public function setRevision($revision) {
    $this->revision = $revision;
  }

  /**
   * @return string
   */
  public function getRevision() {
    return $this->revision;
  }

  /**
   * @param $message
   */
  protected static function error($message) {
    die($message);
  }

  /**
   * Load the class object
   *
   * @param $id
   */
  protected function load($id) {
    $this->error(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden.');
  }

  /**
   * Saves the class object
   */
  protected function save() {
    $this->error(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden.');
  }

  /**
   * @param $id
   */
  public function __construct($id = FALSE) {
    if (FALSE !== $id) {
      $this->load($id);
    }
  }

  /**
   *
   */
  protected function loadChanges() {
    $this->error(__CLASS__ . '::' . __FUNCTION__ . ' should be overridden.');
  }

}
