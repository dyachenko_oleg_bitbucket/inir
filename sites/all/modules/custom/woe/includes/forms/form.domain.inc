<?php

/**
 * @param $form
 * @param $form_state
 * @param $domain
 *
 * @return array
 */
function woe_domain_form($form, &$form_state, Domain $domain) {

  $form = array(
    '#tree' => TRUE,
  );

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $domain ? $domain->getId() : 0,
  );

  $form['name'] = array(
    '#type'  => 'textfield',
    '#title' => 'Name',
    '#value' => $domain->getName(),
  );

  $type = $domain ? $domain->getType() : 'string';
  $form['type'] = array(
    '#type'    => 'select',
    '#title'   => t('Type'),
    '#options' => array(
      'bool'   => t('Boolean'),
      'int'    => t('Integer'),
      'float'  => t('Float'),
      'string' => t('String'),
      'date'   => t('Date'),
      'enum'   => t('Enum'),
    ),
    '#value'   => $type,
    '#ajax'    => array(
      'event'    => 'change',
      'callback' => 'woe_domain_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  $form['settings'] = array(
    '#type'   => 'fieldset',
    '#title'  => 'enum' == $type ? t('Values') : t('Settings'),
    '#prefix' => '<div id="settings-div">',
    '#suffix' => '</div>',
  );

  if (empty($form_state['settings_count'])) {
    $form_state['settings_count'] = 1;
  }
  $settings = array();
  if ($domain) {
    // todo enum values
    if ($settings = /*'enum' == $type ? $domain->getValues() :*/
      $domain->getSettings()
    ) {
      $form_state['settings_count'] = count($settings);
    }
  }

  for ($i = 0; $i < $form_state['settings_count']; $i++) {
    $form['settings']['option'][$i] = array(
      '#type' => 'textfield',
    );
    if (!$domain->processed) {
      $settings = /*'enum' == $type ? $domain->getValues() :*/
        $domain->getSettings();
      $form['settings']['option'][$i]['#value'] = isset($settings[$i]) ? $settings[$i] : '';
    }
  }
  $form['settings']['more'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add more'),
    '#submit' => array('woe_domain_settings_more'),
    '#ajax'   => array(
      'callback' => 'woe_domain_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  if ($form_state['settings_count'] > 1) {
    $form['settings']['remove'] = array(
      '#type'   => 'submit',
      '#value'  => t('Remove one'),
      '#submit' => array('woe_domain_settings_none'),
      '#ajax'   => array(
        'callback' => 'woe_domain_settings_ajax',
        'wrapper'  => 'settings-div',
      ),
    );
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  $domain->processed = TRUE;

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function woe_domain_settings_ajax(&$form, $form_state) {
  $type = $form_state['input']['type'];
  $form['settings']['#title'] = 'enum' == $type ? 'Values' : 'Settings';
  return $form['settings'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function woe_domain_settings_more($form, &$form_state) {
  $form_state['settings_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_domain_settings_none($form, &$form_state) {
  $form_state['settings_count']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_domain_form_validate($form, &$form_state) {
  // todo domain validation
}

/**
 * @param $form
 * @param $form_state
 */
function woe_domain_form_submit($form, &$form_state) {
  $values = $form_state['input'];

  if (woe_syncronous()) {
    // todo
  }
  else {
    $revision = new Revision();
    $revision_changes = new RevisionChanges($revision);

    // id
    $id = intval($values['id']);
    if (!$id) {
      // create new object
      $object = new Domain();
      $object->setRevision($revision->getRevision());
      $object->save();

      // new object
      $revision_changes->addChanges(array(
        'oid'    => $object->getId(),
        'otype'  => 'domain',
        'action' => 'create',
        'field'  => '',
        'delta'  => 0,
        'value'  => '',
      ));
    }
    else {
      $object = new Domain($id);

      // new object
      $revision_changes->addChanges(array(
        'oid'    => $object->getId(),
        'otype'  => 'domain',
        'action' => 'update',
        'field'  => '',
        'delta'  => 0,
        'value'  => '',
      ));
    }

    // name
    if ($object->getName() != $values['name']) {
      $revision_changes->addChanges(array(
        'oid'    => $object->getId(),
        'otype'  => 'domain',
        'action' => 'update',
        'field'  => 'name',
        'delta'  => 0,
        'value'  => $values['name'],
      ));
    }

    // type
    if ($object->getType() != $values['type']) {
      $revision_changes->addChanges(array(
        'oid'    => $object->getId(),
        'otype'  => 'domain',
        'action' => 'update',
        'field'  => 'type',
        'delta'  => 0,
        'value'  => $values['type'],
      ));

      // todo promote changes

      // remove settings for new enums
      if ('enum' == $values['type']) {
        if ($options = $object->getSettings()) {
          $revision_changes->addChanges(array(
            'oid'    => $object->getId(),
            'otype'  => 'domain',
            'action' => 'update',
            'field'  => 'settings',
            'delta'  => 0,
            'value'  => $options,
          ));
        }
      }

      // remove values for old enums
      if ('enum' == $object->getType()) {
        if ($options = $object->getValues()) {
          foreach ($options as $index => $option) {
            $revision_changes->addChanges(array(
              'oid'    => $object->getId(),
              'otype'  => 'domain',
              'action' => 'delete',
              'field'  => 'values',
              'delta'  => $index,
              'value'  => $option,
            ));
          }
        }
      }
    }

    // options
    // $options = array_filter($values['settings']['option']);
    $options = $values['settings']['option'];
    if ('enum' == $values['type']) {

    }
    else {
      if (serialize($options) != serialize($object->getSettings())) {
        $revision_changes->addChanges(array(
          'oid'    => $object->getId(),
          'otype'  => 'domain',
          'action' => 'update',
          'field'  => 'settings',
          'delta'  => 0,
          'value'  => $options,
        ));
      }
    }

    dd($object);

    $change = array();
  }

  exit;

  $domain->setId();
  $domain->setName($values['name']);
  $domain->setType($values['type']);
  $options = array_filter($values['settings']['option']);
  if ('enum' == $domain->getType()) {
    $domain->setValues($options);
  }
  else {
    sort($options);
    $domain->setSettings($options);
  }
  $domain->save();
}


function compare_lists ($list, $list_new) {
  $changes = array();
  $count = max(count($list), count($list_new));
  for ($i=0; $i<$count; $i++) {
    if (isset($list[$i]) && !isset($list_new[$i])) {
      $changes[] = array('action' => 'delete', 'delta');
    }
    if ($list[$i] == $list_new[$i]) {

    }
  }
}
