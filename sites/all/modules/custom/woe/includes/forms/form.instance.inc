<?php

/**
 * @param $form
 * @param $form_state
 * @param $instance
 *
 * @return array
 */
function woe_instance_form($form, &$form_state, $instance) {

  $form = array(
    '#tree' => TRUE,
  );

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $instance ? $instance->getId() : 0,
  );

  $form['eid'] = array(
    '#type'     => 'select',
    '#title'    => 'Entity',
    '#options'  => Entity::getList(),
    '#disabled' => (bool) $instance->getEid(),
    '#ajax'     => array(
      'event'    => 'change',
      'callback' => 'woe_instance_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  $form['instance_attributes'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Attributes'),
    '#prefix' => '<div id="settings-div">',
    '#suffix' => '</div>',
  );

  if ('system' == arg(0) && 'ajax' == arg(1)) {
    $eid = $form_state['values']['eid'];
  }
  else {
    $eid = $instance->getEid();
  }

  $entity = new Entity($eid);
  if ($entity->attributes) {
    foreach ($entity->attributes as $ea) {
      $form['instance_attributes'][$ea->getId()] = array(
        '#type'   => 'fieldset',
        '#title'  => $ea->attribute->getName() . ($ea->getRequired() ? ' - required' : ''),
        '#prefix' => '<div id="ea-' . $ea->attribute->getId() . '-div">',
        '#suffix' => '</div>',
      );

      $values = empty($instance->attributes[$ea->getId()]) ? array() : $instance->attributes[$ea->getId()];
      switch ($ea->attribute->domain->getType()) {
        case 'enum' :
          $form_item = array(
            '#type'          => 'select',
            '#options'       => $ea->attribute->domain->getValues(),
            '#default_value' => array(),
          );
          break;
        case 'bool' :
          $form_item = array(
            '#type'          => 'checkbox',
            '#title'         => 'Yes',
            '#default_value' => NULL,
          );
          break;
        default :
          $form_item = array(
            '#type'          => 'textfield',
            '#default_value' => NULL,
          );
          break;
      }
      if (!is_null($values) && $values) {
        foreach ($values as $index => $ia) {
          $form_item_populated = $form_item;
          if (is_array($form_item_populated['#default_value'])) {
            $form_item_populated['#default_value'][] = $ia->getValue();
          }
          else {
            $form_item_populated['#default_value'] = $ia->getValue();
          }
          $form['instance_attributes'][$ea->getId()]['values'][$index] = $form_item_populated;
        }
      }
      else {
        $form['instance_attributes'][$ea->getId()]['values'][0] = $form_item;
      }

      //d($ea);
      //$form['instance_attributes'][$ea->getId()]
    }
  }


  return $form;
  // todo complete form
  // dd($instance);


  if (empty($form_state['attributes_count'])) {
    $form_state['attributes_count'] = 1;
  }
  $settings = array();
  if (!$instance->processed) {
    $form_state['attributes_count'] = count($instance->attributes);
  }

  for ($i = 0; $i < $form_state['attributes_count']; $i++) {
    $form['instance_attributes'][$i]['attribute'] = array(
      '#type'    => 'select',
      '#title'   => 'Attribute',
      '#options' => Attribute::getList(),
    );

    $form['instance_attributes'][$i]['settings'] = array(
      '#type'          => 'checkboxes',
      '#title'         => 'Settings',
      '#options'       => array(
        'multiple' => 'Multiple',
        'required' => 'Required',
      ),
      '#default_value' => array(),
    );
    $form['instance_attributes'][$i]['raid'] = array(
      '#type'          => 'hidden',
      '#default_value' => 0,
    );

    if (!$instance->processed) {
      // d($instance->attributes[$i]);
      if (isset($instance->attributes[$i])) {
        $form['instance_attributes'][$i]['raid']['#default_value'] = $instance->attributes[$i]->getId();
        $form['instance_attributes'][$i]['attribute']['#default_value'] = $instance->attributes[$i]->getAid();
        if ($instance->attributes[$i]->getMultiple()) {
          $form['instance_attributes'][$i]['settings']['#default_value'][] = 'multiple';
        }
        if ($instance->attributes[$i]->getRequired()) {
          $form['instance_attributes'][$i]['settings']['#default_value'][] = 'required';
        }
      }
    }
  }

  $form['instance_attributes']['more'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add more'),
    '#submit' => array('woe_instance_settings_more'),
    '#ajax'   => array(
      'callback' => 'woe_instance_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  if ($form_state['attributes_count'] > 1) {
    $form['instance_attributes']['remove'] = array(
      '#type'   => 'submit',
      '#value'  => t('Remove one'),
      '#submit' => array('woe_instance_settings_none'),
      '#ajax'   => array(
        'callback' => 'woe_instance_settings_ajax',
        'wrapper'  => 'settings-div',
      ),
    );
  }

  $form['instance_relations'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Relations'),
    '#prefix' => '<div id="relations-div">',
    '#suffix' => '</div>',
  );

  if ('system' == arg(0) && 'ajax' == arg(1)) {
    $instance = new instance($instance->getId());
  }

  $relations_count = count($instance->relations);
  $relations = array_values($instance->relations);

  for ($i = 0; $i < $relations_count; $i++) {

    $form['instance_relations'][$i]['larg'] = array(
      '#type'   => 'item',
      '#title'  => 'Left argument',
      '#markup' => 'instance #' . $relations[$i]->getEid1(),
    );
    $form['instance_relations'][$i]['rarg'] = array(
      '#type'   => 'item',
      '#title'  => 'Right argument',
      '#markup' => 'instance #' . $relations[$i]->getEid2(),
    );
  }

  $form['instance_relations']['refresh'] = array(
    '#type'   => 'button',
    '#value'  => t('refresh'),
    '#submit' => array('woe_instance_relations_rebuild'),
    '#ajax'   => array(
      'callback' => 'woe_instance_relations_ajax',
      'wrapper'  => 'relations-div',
    ),
  );
  $form['instance_relations']['more'] = array(
    '#type'   => 'item',
    '#markup' => l('Add relation', ''),
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  $instance->processed = TRUE;

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function woe_instance_relations_ajax(&$form, $form_state) {
  return $form['instance_relations'];
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function woe_instance_settings_ajax(&$form, $form_state) {
  return $form['instance_attributes'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function woe_instance_relations_rebuild($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function woe_instance_settings_more($form, &$form_state) {
  $form_state['attributes_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_instance_settings_none($form, &$form_state) {
  $form_state['attributes_count']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_instance_form_submit($form, &$form_state) {
  $values = $form_state['input'];

  $instance = new Instance();
  $instance->setId(intval($values['id']));

  if ($values['instance_attributes']) {
    foreach ($values['instance_attributes'] as $attribute) {
      if ($attribute['raid']) {
        $ea = Cache::load('InstanceAttribute', intval($attribute['raid']));
      }
      else {
        $ea = new InstanceAttribute();
      }

      $ea->setMultiple(is_null($attribute['settings']['multiple']) ? 0 : 1);
      $ea->setRequired(is_null($attribute['settings']['required']) ? 0 : 1);
      $ea->setAid(intval($attribute['attribute']));
      $instance->attributes[] = $ea;
    }
  }

  $instance->save();
}
