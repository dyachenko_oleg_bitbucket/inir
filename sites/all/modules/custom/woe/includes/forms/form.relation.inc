<?php

/**
 * @param $form
 * @param $form_state
 * @param $relation
 *
 * @return array
 */
function woe_relation_form($form, &$form_state, $relation) {

  $form = array(
    '#tree' => TRUE,
  );

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $relation ? $relation->getId() : 0,
  );

  $label = Cache::label($relation->getId(), 'relation');
  $form['label'] = array(
    '#type'    => 'textfield',
    '#title'   => t('Label'),
    '#value' => @$label['en'],
  );

  $form['eid1'] = array(
    '#type'    => 'select',
    '#title'   => t('Left argument'),
    '#options' => Entity::getList(),
    '#value'   => $relation ? $relation->getEid1() : 0,
  );

  $form['eid2'] = array(
    '#type'    => 'select',
    '#title'   => t('Left argument'),
    '#options' => Entity::getList(),
    '#value'   => $relation ? $relation->getEid2() : 0,
  );

  $form['settings'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Settings'),
    '#options'       => Relation::availableSettings(),
    '#default_value' => $relation ? $relation->getSettings() : array(),
  );

  // dd($form['settings']);

  $form['relation_attributes'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Attributes'),
    '#prefix' => '<div id="settings-div">',
    '#suffix' => '</div>',
  );

  if (empty($form_state['attributes_count'])) {
    $form_state['attributes_count'] = 1;
  }
  $settings = array();
  if (!$relation->processed) {
    $form_state['attributes_count'] = count($relation->attributes);
  }

  for ($i = 0; $i < $form_state['attributes_count']; $i++) {
    $form['relation_attributes'][$i]['attribute'] = array(
      '#type'    => 'select',
      '#title'   => 'Attribute',
      '#options' => Attribute::getList(),
    );

    $form['relation_attributes'][$i]['settings'] = array(
      '#type'          => 'checkboxes',
      '#title'         => 'Settings',
      '#options'       => array(
        'multiple' => 'Multiple',
        'required' => 'Required',
      ),
      '#default_value' => array(),
    );
    $form['relation_attributes'][$i]['raid'] = array(
      '#type'          => 'hidden',
      '#default_value' => 0,
    );

    if (!$relation->processed) {
      // d($relation->attributes[$i]);
      if (isset($relation->attributes[$i])) {
        $form['relation_attributes'][$i]['raid']['#default_value'] = $relation->attributes[$i]->getId();
        $form['relation_attributes'][$i]['attribute']['#default_value'] = $relation->attributes[$i]->getAid();
        if ($relation->attributes[$i]->getMultiple()) {
          $form['relation_attributes'][$i]['settings']['#default_value'][] = 'multiple';
        }
        if ($relation->attributes[$i]->getRequired()) {
          $form['relation_attributes'][$i]['settings']['#default_value'][] = 'required';
        }
      }
    }
  }

  $form['relation_attributes']['more'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add more'),
    '#submit' => array('woe_relation_settings_more'),
    '#ajax'   => array(
      'callback' => 'woe_relation_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  if ($form_state['attributes_count'] > 1) {
    $form['relation_attributes']['remove'] = array(
      '#type'   => 'submit',
      '#value'  => t('Remove one'),
      '#submit' => array('woe_relation_settings_none'),
      '#ajax'   => array(
        'callback' => 'woe_relation_settings_ajax',
        'wrapper'  => 'settings-div',
      ),
    );
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  $relation->processed = TRUE;

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function woe_relation_settings_ajax(&$form, $form_state) {
  return $form['relation_attributes'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function woe_relation_settings_more($form, &$form_state) {
  $form_state['attributes_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_relation_settings_none($form, &$form_state) {
  $form_state['attributes_count']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_relation_form_submit($form, &$form_state) {
  $values = $form_state['input'];

  $relation = new Relation();
  $relation->setId(intval($values['id']));
  $relation->setEid1(intval($values['eid1']));
  $relation->setEid2(intval($values['eid2']));
  $relation->setSettings(array_values(array_filter($values['settings'])));

  if ($values['relation_attributes']) {
    foreach ($values['relation_attributes'] as $attribute) {
      if ($attribute['raid']) {
        $ra = Cache::load('RelationAttribute', intval($attribute['raid']));
      }
      else {
        $ra = new RelationAttribute();
      }
      // d($attribute['settings']);
      $ra->setMultiple(is_null($attribute['settings']['multiple']) ? 0 : 1);
      $ra->setRequired(is_null($attribute['settings']['required']) ? 0 : 1);
      $ra->setAid(intval($attribute['attribute']));
      $relation->attributes[] = $ra;
    }
  }

  $relation->save();
}
