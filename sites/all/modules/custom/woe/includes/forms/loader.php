<?php

/**
 * @param null   $class
 * @param string $driver
 *
 * @return array|null
 */
function woe_forms($object = NULL) {
  $forms = array(
    'domain'    => 'woe_domain_form',
    'attribute' => 'woe_attribute_form',
    'relation'  => 'woe_relation_form',
    'entity'    => 'woe_entity_form',
    'instance'  => 'woe_instance_form',
  );

  //
  if (is_null($object)) {
    return $forms;
  }

  //
  if (isset($forms[$object])) {
    return $forms[$object];
  }

  return NULL;
}

/**
 *
 */
foreach (woe_forms() as $object => $form_id) {
  require_once __DIR__ . '/form.' . $object . '.inc';
}
