<?php

/**
 * @param $form
 * @param $form_state
 * @param $entity
 *
 * @return array
 */
function woe_entity_form($form, &$form_state, $entity) {

  $form = array(
    '#tree' => TRUE,
  );

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $entity ? $entity->getId() : 0,
  );

  $label = Cache::label($entity->getId(), 'entity');
  $form['label'] = array(
    '#type'    => 'textfield',
    '#title'   => t('Label'),
    '#value' => @$label['en'],
  );

  if (empty($form_state['attributes_count'])) {
    $form_state['attributes_count'] = 1;
  }
  if (!$entity->processed) {
    $form_state['attributes_count'] = count($entity->attributes);
  }

  $form['label_instances'] = array(
    '#type' => 'select',
    '#title'   => t('Attribute used as Label for Instances'),
    '#options' => Attribute::getList(),
    '#value' => @$label['en'],
  );

  $form['entity_attributes'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Attributes'),
    '#prefix' => '<div id="settings-div">',
    '#suffix' => '</div>',
  );

  $attributes = array();
  for ($i = 0; $i < $form_state['attributes_count']; $i++) {
    $form['entity_attributes'][$i]['attribute'] = array(
      '#type'    => 'select',
      '#title'   => 'Attribute',
      '#options' => Attribute::getList(),
    );

    $form['entity_attributes'][$i]['settings'] = array(
      '#type'          => 'checkboxes',
      '#title'         => 'Settings',
      '#options'       => array(
        'multiple' => 'Multiple',
        'required' => 'Required',
      ),
      '#default_value' => array(),
    );
    $form['entity_attributes'][$i]['eaid'] = array(
      '#type'          => 'hidden',
      '#default_value' => 0,
    );

    if (!$entity->processed) {
      if (isset($entity->attributes[$i])) {
        $attributes[ $entity->attributes[$i]->getAid() ] = $entity->attributes[$i]->attribute->getName();
        $form['entity_attributes'][$i]['eaid']['#default_value'] = $entity->attributes[$i]->getId();
        $form['entity_attributes'][$i]['attribute']['#default_value'] = $entity->attributes[$i]->getAid();
        if ($entity->attributes[$i]->getMultiple()) {
          $form['entity_attributes'][$i]['settings']['#default_value'][] = 'multiple';
        }
        if ($entity->attributes[$i]->getRequired()) {
          $form['entity_attributes'][$i]['settings']['#default_value'][] = 'required';
        }
      }
    }
  }

  $form['label_instances']['#options'] = $attributes;

  $form['entity_attributes']['more'] = array(
    '#type'   => 'submit',
    '#value'  => t('Add more'),
    '#submit' => array('woe_entity_settings_more'),
    '#ajax'   => array(
      'callback' => 'woe_entity_settings_ajax',
      'wrapper'  => 'settings-div',
    ),
  );

  if ($form_state['attributes_count'] > 1) {
    $form['entity_attributes']['remove'] = array(
      '#type'   => 'submit',
      '#value'  => t('Remove one'),
      '#submit' => array('woe_entity_settings_none'),
      '#ajax'   => array(
        'callback' => 'woe_entity_settings_ajax',
        'wrapper'  => 'settings-div',
      ),
    );
  }

  $form['entity_relations'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Relations'),
    '#prefix' => '<div id="relations-div">',
    '#suffix' => '</div>',
  );

  if ('system' == arg(0) && 'ajax' == arg(1)) {
    $entity = new Entity($entity->getId());
  }

  $relations_count = count($entity->relations);
  $relations = array_values($entity->relations);

  for ($i = 0; $i < $relations_count; $i++) {

    $form['entity_relations'][$i]['#prefix'] = '<div style="border:1px #ccc solid; padding: 0 10px; margin: 10px 0;">';
    $form['entity_relations'][$i]['#suffix'] = '</div>';
    $form['entity_relations'][$i]['arg'] = array(
      '#type'   => 'item',
      '#title'  => 'Name',
      '#markup' => Cache::label($relations[$i]->getId(), 'relation'),
    );
    $form['entity_relations'][$i]['larg'] = array(
      '#type'   => 'item',
      '#title'  => 'Left argument',
      '#markup' => Cache::label($relations[$i]->getEid1(), 'entity'),
    );
    $form['entity_relations'][$i]['rarg'] = array(
      '#type'   => 'item',
      '#title'  => 'Right argument',
      '#markup' => Cache::label($relations[$i]->getEid2(), 'entity'),
    );
  }

  $form['entity_relations']['refresh'] = array(
    '#type'   => 'button',
    '#value'  => t('refresh'),
    '#submit' => array('woe_entity_relations_rebuild'),
    '#ajax'   => array(
      'callback' => 'woe_entity_relations_ajax',
      'wrapper'  => 'relations-div',
    ),
  );
  $form['entity_relations']['more'] = array(
    '#type'   => 'item',
    '#markup' => l('Add relation', ''),
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  $entity->processed = TRUE;

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function woe_entity_relations_ajax(&$form, $form_state) {
  return $form['entity_relations'];
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function woe_entity_settings_ajax(&$form, $form_state) {
  return $form['entity_attributes'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function woe_entity_relations_rebuild($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function woe_entity_settings_more($form, &$form_state) {
  $form_state['attributes_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_entity_settings_none($form, &$form_state) {
  $form_state['attributes_count']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * @param $form
 * @param $form_state
 */
function woe_entity_form_submit($form, &$form_state) {
  $values = $form_state['input'];

  $entity = new Entity();
  $entity->setId(intval($values['id']));

  if ($values['entity_attributes']) {
    foreach ($values['entity_attributes'] as $attribute) {
      if ($attribute['raid']) {
        $ea = Cache::load('entityAttribute', intval($attribute['eaid']));
      }
      else {
        $ea = new EntityAttribute();
      }

      $ea->setMultiple(is_null($attribute['settings']['multiple']) ? 0 : 1);
      $ea->setRequired(is_null($attribute['settings']['required']) ? 0 : 1);
      $ea->setAid(intval($attribute['attribute']));
      $entity->attributes[] = $ea;
    }
  }

  $entity->save();
}
